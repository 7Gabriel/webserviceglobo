package br.com.webservicecrawler.controller;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.crawlerglobo.model.Feed;
import br.com.crawlerglobo.parser.ConvertToJson;
import br.com.crawlerglobo.parser.LeitorRSSFeedGlobo;

/**
 * @author jgosilva
 *
 */
@Path("/service")
@RolesAllowed({"ADMIN"})
public class CrawlerServiceController {

	private final String URL = "http://revistaautoesporte.globo.com/rss/ultimas/feed.xml";
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Produces("application/json; charset=UTF-8")
	@Path("/feed")
	public String getRSSFeedGlobo(){
		LeitorRSSFeedGlobo parser = new LeitorRSSFeedGlobo(URL);
		Feed feed = parser.readFeed();
		String xstream = ConvertToJson.convertParaJson(feed);
		return xstream;
	}
}
